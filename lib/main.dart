import 'package:flutter/material.dart';
import 'package:map_view/map_view.dart';

var apiKey = "AIzaSyB1XcslaNpOKZ5MpeXD4zIfPivtcELUY5U";
void main() {
  MapView.setApiKey(apiKey);
  runApp(new MaterialApp(
    home: new MapPage(),
  ));
}
class MapPage extends StatefulWidget {
  @override
  _MapPageState createState() => new _MapPageState();
}

class _MapPageState extends State<MapPage> with SingleTickerProviderStateMixin {
  MapView mapView = new MapView();
  List<Marker> _markers = <Marker>[
    new Marker("1", "Airplane Park", 14.875938, 102.043528, color: Colors.blue),
    new Marker("2", "Takeoff Cafe", 14.875660, 102.043080, color: Colors.red),
  ];

  onUserLocationUpdated(position,MapView mapview)async {
    double zoomLevel = await mapview.zoomLevel;
    mapview.setCameraPosition(position.latitude, position.longitude, zoomLevel);
  } //Updated user locations

  showMap() {
    mapView.show(
      new MapOptions(
        mapViewType: MapViewType.hybrid,
        initialCameraPosition: new CameraPosition(Location(14.875660, 102.043080), 18.0),
        showUserLocation: true,
        title: "My Location"
      )
    );
    
    mapView.onLocationUpdated.listen((position)=>this.onUserLocationUpdated(position,mapView)); //Track user locations
    mapView.onMapTapped.listen((location) => mapView.addMarker(new Marker("", "Mark", location.latitude, location.longitude)));
    mapView.onTouchAnnotation.listen((marker) => mapView.removeMarker(marker));
    mapView.onMapReady.listen((_){
      setState(() {
         mapView.setMarkers(_markers); //Set markers
      });
    });
  }

  AnimationController controller;
  Animation<double> animation;
  @override
  void initState() {
    super.initState();
    controller = new AnimationController(duration: new Duration(milliseconds: 1000), vsync: this);
    animation = new CurvedAnimation(parent: controller, curve: Curves.bounceInOut);
    animation.addListener(() {
      this.setState(() {});
    });
    animation.addStatusListener((AnimationStatus status) {
    });
    controller.repeat();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(title: Text("Map Demo"),),
      backgroundColor: Colors.lightBlueAccent,
      body: new Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          new Container(
            child: new Stack(
              children: <Widget>[
                  new Container(
                    child: new Center(
                      child: new IconButton(
                        icon: new Icon(Icons.map, color: Colors.blueAccent),
                        iconSize: animation.value * 120.0,
                        onPressed: showMap,
                      ),
                    ),
                  )
              ],
            ),
          ),
          new Container(
            padding: new EdgeInsets.only(bottom: 15.0),
            child: new Text(
              "Tap to view map",
              style: new TextStyle(color: Colors.white,fontSize: animation.value * 35.0 ,fontWeight: FontWeight.bold)
            ),
          ),
        ],
      ),
    );
  }
}